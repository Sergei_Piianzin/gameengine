package org.engine;

import org.engine.buffer.VertexBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Vector3f;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class Mesh {
    private Vertex[] vertices;
    private byte[] indices;
    private Texture texture;
    private VertexBuffer vertexBuffer;
    private Vector3f modelPos;
    private Vector3f modelAngle;
    private Vector3f modelScale;

    public Mesh(Vertex[] vertices, byte[] indices) {
        this.vertices = vertices;
        this.indices = indices;
        this.texture = new Texture();
        this.vertexBuffer = new VertexBuffer();
        modelPos = new Vector3f(0, 0, 5.0f);
        modelAngle = new Vector3f(0, 0, 0);
        modelScale = new Vector3f(1, 1, 1);
    }

    public void draw(int programId) {
        vertexBuffer.draw(indices.length, texture.getTexId(), programId);
    }

    public void generateVertexArray() {
        vertexBuffer.generateVertexArray(this.getVertexBuffer(), this.getIndicesBuffer());
    }

    public FloatBuffer getVertexBuffer() {
        FloatBuffer commonBuffer = BufferUtils.createFloatBuffer(Vertex.elementCount * vertices.length);
        for(Vertex v : vertices) {
            commonBuffer.put(v.getAllElements());
        }
        commonBuffer.flip();
        return commonBuffer;
    }
    
    public ByteBuffer getIndicesBuffer() {
    	ByteBuffer indicesBuffer = BufferUtils.createByteBuffer(indices.length);
    	indicesBuffer.put(indices);
    	indicesBuffer.flip();
    	return indicesBuffer;
    }

    public void bindTexture() {
        texture.bindTexture();
    }

    public Vertex[] getVertices() {
        return vertices;
    }

    public byte[] getIndices() {
        return indices;
    }

    public int getTextureId() {
       return texture.getTexId();
    }

    public Texture getTexture() {
        return texture;
    }

    public Vector3f getModelPos() {
        return modelPos;
    }

    public Vector3f getModelAngle() {
        return modelAngle;
    }

    public Vector3f getModelScale() {
        return modelScale;
    }

    public void setModelPos(Vector3f modelPos) {
        this.modelPos = modelPos;
    }

    public void setModelAngle(Vector3f modelAngle) {
        this.modelAngle = modelAngle;
    }

    public void setModelScale(Vector3f modelScale) {
        this.modelScale = modelScale;
    }
}
