package org.engine;

public class Vertex {
    private float[] xyzw = new float[] {0f, 0f, 0f, 1f};
    private float[] rgba = new float[] {1f, 1f, 1f, 1f};
    private float[] st = new float[] {0f, 0f};
    // The amount of bytes an element has
    public static final int elementBytes = 4;
    // Elements per parameter
    public static final int positionElementCount = 4;
    public static final int colorElementCount = 4;
    public static final int textureElementCount = 2;
    // Bytes per parameter
    public static final int positionBytesCount = positionElementCount * elementBytes;
    public static final int colorByteCount = colorElementCount * elementBytes;
    public static final int textureByteCount = textureElementCount * elementBytes;
    // Byte offsets per parameter
    public static final int positionByteOffset = 0;
    public static final int colorByteOffset = positionByteOffset + positionBytesCount;
    public static final int textureByteOffset = colorByteOffset + colorByteCount;
    // The amount of elements that a vertex has
    public static final int elementCount = positionElementCount +
            colorElementCount + textureElementCount;

    public static final int sizeInBytes = positionBytesCount + colorByteCount + textureByteCount;
    public Vertex() {}

    public Vertex(float[] xyzw, float[] rgba, float[] st) {
        this.xyzw = xyzw;
        this.rgba = rgba;
        this.st = st;
    }

    public float[] getPosColor() {
        float[] out = new float[positionElementCount+colorElementCount];
        int i=0;
        out[i++] = this.xyzw[0];
        out[i++] = this.xyzw[1];
        out[i++] = this.xyzw[2];
        out[i++] = this.xyzw[3];
        out[i++] = this.rgba[0];
        out[i++] = this.rgba[1];
        out[i++] = this.rgba[2];
        out[i++] = this.rgba[3];
        return out;
    }

    public float[] getAllElements() {
        float[] out = new float[elementCount];
        int i = 0;
        out[i++] = this.xyzw[0];
        out[i++] = this.xyzw[1];
        out[i++] = this.xyzw[2];
        out[i++] = this.xyzw[3];
        out[i++] = this.rgba[0];
        out[i++] = this.rgba[1];
        out[i++] = this.rgba[2];
        out[i++] = this.rgba[3];
        out[i++] = this.st[0];
        out[i++] = this.st[1];
        return out;
    }

    public void setXyzw(float[] xyzw) {
        this.xyzw = xyzw;
    }

    public void setRgba(float[] rgba) {
        this.rgba = rgba;
    }

    public void setSt(float[] st) {
        this.st = st;
    }

    public float[] getXyzw() {
        return xyzw;
    }

    public float[] getRgba() {
        return rgba;
    }

    public float[] getSt() {
        return st;
    }
}
