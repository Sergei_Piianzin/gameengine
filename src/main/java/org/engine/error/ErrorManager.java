package org.engine.error;

import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorManager {
    private static Logger logger = LoggerFactory.getLogger(ErrorManager.class);

	public static void exitOnGLError(String errorMessage) {
        int errorValue = GL11.glGetError();

        if (errorValue != GL11.GL_NO_ERROR) {
            String errorString = GLU.gluErrorString(errorValue);
            logger.error("{} : {}", errorMessage, errorString);

            if (Display.isCreated()) Display.destroy();
            System.exit(-1);
        }
    }
}
