package org.engine.display;

public class DisplaySettings {
  private int width;
  private int height;
  private int fpsSync;
  private String displayTitle;
  
  public DisplaySettings(int width, int height, int fpsSync, String displayTitle) {
    this.width = width;
    this.height = height;
    this.fpsSync = fpsSync;
    this.displayTitle = displayTitle;
  }
  
  public int getWidth() {
    return width;
  }
  
  public int getHeight() {
    return height;
  }
  
  public int getFpsSync() {
    return fpsSync;
  }
  
  public String getDisplayTitle() {
    return displayTitle;
  }
}