package org.engine.display;

import org.engine.scene.Camera;
import org.engine.Mesh;
import org.engine.ShaderProgram;
import org.engine.scene.Scene;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.*;
import org.lwjgl.util.vector.Vector3f;

public class DisplayManager {
  private DisplaySettings displaySettings;
  
  public DisplayManager(DisplaySettings displaySettings) {
    this.displaySettings = displaySettings;
  }

  public void initDisplay() {
      try {
          PixelFormat pixelFormat = new PixelFormat();
          ContextAttribs contextAttributes =
                  new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true);

          Display.setDisplayMode(new DisplayMode(displaySettings.getWidth(), displaySettings.getHeight()));
          Display.setTitle(displaySettings.getDisplayTitle());
          Display.create(pixelFormat, contextAttributes);

          GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
          GL11.glEnable(GL11.GL_CULL_FACE);
          GL11.glEnable(GL11.GL_DEPTH_TEST);
      } catch (LWJGLException e) {
          e.printStackTrace();
          System.exit(-1);
      }
  }

  public void showDisplay(Mesh[] meshes) {
      for (Mesh mesh : meshes) {
          mesh.generateVertexArray();
          mesh.bindTexture();
      }

      ShaderProgram shaderProgram = new ShaderProgram();
      shaderProgram.setupShaders();

    Vector3f position = new Vector3f(0.0f, 0.0f, 0.0f);
    Vector3f up = new Vector3f(0.0f, 1.0f, 0.0f);
    Vector3f target = new Vector3f(0.0f, 0.0f, 1.0f);

    Camera camera = new Camera(position, up, target, displaySettings);
    Scene scene = new Scene(camera, meshes);

    while (!Display.isCloseRequested()) {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        scene.drawMeshes(shaderProgram);
        Display.sync(displaySettings.getFpsSync());
        Display.update();
    }
  }

  public void closeDisplay() {
    Display.destroy();
  }
}
