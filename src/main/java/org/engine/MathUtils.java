package org.engine;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

public class MathUtils {
    public static final double PI = 3.14159265358979323846;

    public static float coTangent(float angle) {
        return (float)(1f / Math.tan(angle));
    }

    public static float degreesToRadians(float degrees) {
        return degrees * (float)(PI / 180d);
    }

    public static float radToDegrees(float rad) {return rad * (float)(180d/PI); }

    public static Matrix4f mul(Matrix4f left, Matrix4f right) {
        Matrix4f mul = new Matrix4f();

        mul.m00 = left.m00*right.m00 + left.m01*right.m10+left.m02*right.m20+left.m03*right.m30;
        mul.m01 = left.m00*right.m01 + left.m01*right.m11+left.m02*right.m21+left.m03*right.m31;
        mul.m02 = left.m00*right.m02 + left.m01*right.m12+left.m02*right.m22+left.m03*right.m32;
        mul.m03 = left.m00*right.m03 + left.m01*right.m13+left.m02*right.m23+left.m03*right.m33;

        mul.m10 = left.m10*right.m00 + left.m11*right.m10+left.m12*right.m20+left.m13*right.m30;
        mul.m11 = left.m10*right.m01 + left.m11*right.m11+left.m12*right.m21+left.m13*right.m31;
        mul.m12 = left.m10*right.m02 + left.m11*right.m12+left.m12*right.m22+left.m13*right.m32;
        mul.m13 = left.m10*right.m03 + left.m11*right.m13+left.m12*right.m23+left.m13*right.m33;

        mul.m20 = left.m20*right.m00 + left.m21*right.m10+left.m22*right.m20+left.m23*right.m30;
        mul.m21 = left.m20*right.m01 + left.m21*right.m11+left.m22*right.m21+left.m23*right.m31;
        mul.m22 = left.m20*right.m02 + left.m21*right.m12+left.m22*right.m22+left.m23*right.m32;
        mul.m23 = left.m20*right.m03 + left.m21*right.m13+left.m22*right.m23+left.m23*right.m33;

        mul.m30 = left.m30*right.m00 + left.m31*right.m10+left.m32*right.m20+left.m33*right.m30;
        mul.m31 = left.m30*right.m01 + left.m31*right.m11+left.m32*right.m21+left.m33*right.m31;
        mul.m32 = left.m30*right.m02 + left.m31*right.m12+left.m32*right.m22+left.m33*right.m32;
        mul.m33 = left.m30*right.m03 + left.m31*right.m13+left.m32*right.m23+left.m33*right.m33;

        return mul;
    }

    public static String toString(Matrix4f mat) {
        StringBuilder buf = new StringBuilder();
        buf.append(mat.m00).append(' ').append(mat.m01).append(' ').append(mat.m02).append(' ').append(mat.m03).append('\n');
        buf.append(mat.m10).append(' ').append(mat.m11).append(' ').append(mat.m12).append(' ').append(mat.m13).append('\n');
        buf.append(mat.m20).append(' ').append(mat.m21).append(' ').append(mat.m22).append(' ').append(mat.m23).append('\n');
        buf.append(mat.m30).append(' ').append(mat.m31).append(' ').append(mat.m32).append(' ').append(mat.m33).append('\n');
        return buf.toString();
    }

    public static void rotate(Vector3f src, float angle, Vector3f axe) {
        float SinHalfAngle = (float)Math.sin(MathUtils.degreesToRadians(angle/2));
        float CosHalfAngle = (float)Math.cos(MathUtils.degreesToRadians(angle/2));

        float Rx = axe.x * SinHalfAngle;
        float Ry = axe.y * SinHalfAngle;
        float Rz = axe.z * SinHalfAngle;
        float Rw = CosHalfAngle;
        Quaternion rotationQ = new Quaternion(Rx, Ry, Rz, Rw);

        Quaternion conjugateQ = new Quaternion();
        Quaternion.negate(rotationQ, conjugateQ);
        //  ConjugateQ.Normalize();
        Quaternion W = new Quaternion();
        Quaternion.mul(W, rotationQ , W);
        W = MathUtils.mulQuatByVec3f(W, src);
        Quaternion.mul(W, conjugateQ , W);

        src.set(W.x, W.y, W.z);
    }

    public static Quaternion mulQuatByVec3f(Quaternion q, Vector3f v) {
        float w = - (q.x * v.x) - (q.y * v.y) - (q.z * v.z);
        float x =   (q.w * v.x) + (q.y * v.z) - (q.z * v.y);
        float y =   (q.w * v.y) + (q.z * v.x) - (q.x * v.z);
        float z =   (q.w * v.z) + (q.x * v.y) - (q.y * v.x);

        return new Quaternion(x, y, z, w);
    }
}
