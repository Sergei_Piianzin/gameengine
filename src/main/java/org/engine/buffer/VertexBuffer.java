package org.engine.buffer;

import org.engine.Vertex;
import org.lwjgl.opengl.*;
import org.engine.Mesh;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class VertexBuffer {
    private int vaoId;
    private int vboiId;
    private int vcboId;

    public VertexBuffer() {

    }

    public void generateVertexBuffer(FloatBuffer vertexBuffer) {
        vcboId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vcboId);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertexBuffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(0, Vertex.positionElementCount, GL11.GL_FLOAT, false, Vertex.sizeInBytes, Vertex.positionByteOffset);
        GL20.glVertexAttribPointer(1, Vertex.colorElementCount, GL11.GL_FLOAT, false, Vertex.sizeInBytes, Vertex.colorByteOffset);
        GL20.glVertexAttribPointer(2, Vertex.textureElementCount, GL11.GL_FLOAT, false, Vertex.sizeInBytes, Vertex.textureByteOffset);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
    }

    public void generateIndicesBuffer(ByteBuffer indicesBuffer) {
        vboiId = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    public void generateVertexArray(FloatBuffer vertexBuffer, ByteBuffer indicesBuffer) {
        vaoId = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vaoId);
        generateVertexBuffer(vertexBuffer);
        GL30.glBindVertexArray(0);
        generateIndicesBuffer(indicesBuffer);
    }

    public void draw(int indicesLength, int textureId, int pId) {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);

        GL20.glUseProgram(pId);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);

        // Bind to the VAO that has all the information about the vertices
        GL30.glBindVertexArray(vaoId);
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL20.glEnableVertexAttribArray(2);

        // Bind to the index VBO that has all the information about the order of the vertices
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vboiId);

        // Draw the vertices
        GL11.glDrawElements(GL11.GL_TRIANGLES, indicesLength, GL11.GL_UNSIGNED_BYTE, 0);

        // Put everything back to default (deselect)
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL20.glDisableVertexAttribArray(2);
        GL30.glBindVertexArray(0);
        GL20.glUseProgram(0);
    }

    public int getVaoId() {
        return vaoId;
    }

    public int getVboiId() {
        return vboiId;
    }

    public int getVcboId() {return vcboId; }
}
