package org.engine;

public class ResourceResolver {

    public static String getResourcePath(String resource) {
        return ResourceResolver.class.getClassLoader().getResource(resource).getPath();
    }
}
