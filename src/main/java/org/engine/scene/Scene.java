package org.engine.scene;

import org.engine.MathUtils;
import org.engine.Mesh;
import org.engine.ShaderProgram;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.FloatBuffer;

public class Scene {
    private Logger logger = LoggerFactory.getLogger(Scene.class);
    private boolean printMouse = false;
    private Camera camera;
    private Mesh[] meshes;
    private float step = 0.0f;

    public Scene(Camera camera, Mesh[] meshes) {
        this.camera = camera;
        this.meshes = meshes;
    }

    public void drawMesh(Mesh mesh, ShaderProgram shaderProgram) {
        renderLogic(mesh, shaderProgram);
        mesh.draw(shaderProgram.getpId());
    }

    public void drawMeshes(ShaderProgram shaderProgram) {
        for (Mesh mesh : meshes) {
            renderLogic(mesh, shaderProgram);
            mesh.draw(shaderProgram.getpId());
        }
    }

    private void renderLogic(Mesh mesh, ShaderProgram shaderProgram) {

        float scaleDelta = 0.1f;
        step += 1.0f;

        while(Keyboard.next()) {
            // Only listen to events where the key was pressed (down event)
            if (!Keyboard.getEventKeyState()) continue;
            // Change model scale, rotation and translation values
            switch (Keyboard.getEventKey()) {
                // Move
                case Keyboard.KEY_UP:
                    camera.stepForward(1.0f);
                    break;
                case Keyboard.KEY_DOWN:
                    camera.stepBack(1.0f);
                    break;
                // Rotation
                case Keyboard.KEY_LEFT:
                    camera.stepLeft(1.0f);
                    break;
                case Keyboard.KEY_RIGHT:
                    camera.stepRight(1.0f);
                    break;
                case Keyboard.KEY_S:
                    printMouse = !printMouse;
                    break;
                case Keyboard.KEY_Z:
                    camera.clearPosition();
                    break;
            }
        }

        if (printMouse) {
            camera.onRender(Mouse.getX(), Mouse.getY());
        }

        //mesh.setModelAngle(new Vector3f(0.0f,step, 0.0f));

        // Upload matrices to the uniform variables
        GL20.glUseProgram(shaderProgram.getpId());

        Matrix4f proj = camera.getProjectionMatrix();
        Matrix4f model = camera.createModelMatrix(mesh);
        Matrix4f cameraTranslation = camera.createViewMatrix();
        Matrix4f cameraRotate = camera.getCameraTranslateMatrix();

        Matrix4f MVP = new Matrix4f();
        MVP = MathUtils.mul(MVP, proj);
        MVP = MathUtils.mul(MVP, cameraRotate);
        MVP = MathUtils.mul(MVP, cameraTranslation);
        MVP = MathUtils.mul(MVP, model);

        FloatBuffer matrix44Buffer = BufferUtils.createFloatBuffer(16);

        MVP.store(matrix44Buffer);
        matrix44Buffer.flip();
        GL20.glUniformMatrix4(shaderProgram.getVariableLocation(shaderProgram.getpId(), "MVP"), true, matrix44Buffer);

        GL20.glUseProgram(0);

        ShaderProgram.exitOnGLError("logicCycle");

    }
}
