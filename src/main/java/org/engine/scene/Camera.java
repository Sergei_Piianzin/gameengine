package org.engine.scene;

import org.engine.MathUtils;
import org.engine.Mesh;
import org.engine.display.DisplaySettings;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Camera {
    private static final Logger logger = LoggerFactory.getLogger(Camera.class);
    private final DisplaySettings displaySettings;
    private Matrix4f projectionMatrix;
    private Vector3f position;
    private Vector3f up;
    private Vector3f target;
    private float hAngle;
    private float vAngle;
    private static final int MARGIN = 50;
    private Vector2f mousePosition;

    public Camera(Vector3f position, Vector3f up, Vector3f target, DisplaySettings displaySettings) {
        this.position = position;
        this.up = up.normalise(null);
        this. target = target.normalise(null);
        this.projectionMatrix = createProjectionMatrix(displaySettings.getWidth(), displaySettings.getHeight());
        this.displaySettings = displaySettings;
        init();
    }

    public void stepForward(float step) {
        Vector3f add = new Vector3f(target);
        add.scale(step);
        position.translate(add.x, add.y, add.z);
    }

    public void stepBack(float step) {
        Vector3f add = new Vector3f(target);
        add.scale(step);
        position.translate(-add.x, -add.y, -add.z);
    }

    public void stepLeft(float step) {
        Vector3f left = Vector3f.cross(target,up, null);
        left.normalise(left);
        left.scale(step);

        Vector3f.add(position, left, position);
    }

    public void stepRight(float step) {
        Vector3f right =  Vector3f.cross(up, target ,null);
        right.normalise(right);
        right.scale(step);

        Vector3f.add(position, right, position);
    }

    private Matrix4f createProjectionMatrix(int width, int height) {
        Matrix4f projectionMatrix = new Matrix4f();
        float fieldOfView = 60f;
        float aspectRatio = (float) width / (float) height;
        float near_plane = 0.1f;
        float far_plane = 100.0f;

        float y_scale = MathUtils.coTangent(MathUtils.degreesToRadians(fieldOfView / 2f));
        float x_scale = y_scale / aspectRatio;
        float frustum_length = far_plane - near_plane;

        projectionMatrix.m00 =  x_scale;
        projectionMatrix.m11 =  y_scale;
        projectionMatrix.m22 =  -(far_plane + near_plane) / frustum_length;
        projectionMatrix.m23 = (2 * near_plane * far_plane )/ frustum_length;
        projectionMatrix.m32 = 1;
        projectionMatrix.m33 = 0;

        return projectionMatrix;

    }

    private Matrix4f createCameraTransformationMatrix() {
        Vector3f N = new Vector3f(target);
        N.normalise(N);
        Vector3f U = new Vector3f(up);
        U.normalise(U);

        U = Vector3f.cross(U,N, null);
        Vector3f V = Vector3f.cross(N,U, null);

        Matrix4f cameraMatrix = new Matrix4f();
        cameraMatrix.m00 = U.x; cameraMatrix.m01 = U.y; cameraMatrix.m02 = U.z; cameraMatrix.m03 = 0.0f;
        cameraMatrix.m10 = V.x; cameraMatrix.m11 = V.y; cameraMatrix.m12 = V.z; cameraMatrix.m13 = 0.0f;
        cameraMatrix.m20 = N.x; cameraMatrix.m21 = N.y; cameraMatrix.m22 = N.z; cameraMatrix.m23 = 0.0f;
        cameraMatrix.m30 = 0.0f; cameraMatrix.m31 = 0.0f; cameraMatrix.m32 = 0.0f; cameraMatrix.m33 = 1.0f;

        return cameraMatrix;
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public DisplaySettings getDisplaySettings() {
        return displaySettings;
    }

    public Matrix4f getCameraTranslateMatrix() {
        return createCameraTransformationMatrix();
    }

    public Matrix4f createModelMatrix(Mesh mesh) {
        Matrix4f modelMatrix = new Matrix4f();

        Matrix4f TranslationTrans = new Matrix4f();
        TranslationTrans.m03 = mesh.getModelPos().x;
        TranslationTrans.m13 = mesh.getModelPos().y;
        TranslationTrans.m23 = mesh.getModelPos().z;

        Matrix4f ScaleTrans = new Matrix4f();
        ScaleTrans.m00 = mesh.getModelScale().x;
        ScaleTrans.m11 = mesh.getModelScale().y;
        ScaleTrans.m22 = mesh.getModelScale().z;

        Matrix4f RotateTrans = new Matrix4f();
        Matrix4f rx = new Matrix4f();
        Matrix4f ry = new Matrix4f();
        Matrix4f rz = new Matrix4f();
        float x = MathUtils.degreesToRadians(mesh.getModelAngle().x);
        float y = MathUtils.degreesToRadians(mesh.getModelAngle().y);
        float z = MathUtils.degreesToRadians(mesh.getModelAngle().z);

        rx.m00 = 1.0f; rx.m01 = 0.0f   ; rx.m02 = 0.0f    ; rx.m03 = 0.0f;
        rx.m10 = 0.0f; rx.m11 = (float)Math.cos(x); rx.m12 = -(float)Math.sin(x); rx.m13 = 0.0f;
        rx.m20 = 0.0f; rx.m21 = (float)Math.sin(x); rx.m22 = (float)Math.cos(x) ; rx.m23 = 0.0f;
        rx.m30 = 0.0f; rx.m31 = 0.0f   ; rx.m32 = 0.0f    ; rx.m33 = 1.0f;

        ry.m00 = (float)Math.cos(y); ry.m01 = 0.0f; ry.m02 = -(float)Math.sin(y); ry.m03 = 0.0f;
        ry.m10 = 0.0f   ; ry.m11 = 1.0f; ry.m12 = 0.0f    ; ry.m13 = 0.0f;
        ry.m20 = (float)Math.sin(y); ry.m21 = 0.0f; ry.m22 = (float)Math.cos(y) ; ry.m23 = 0.0f;
        ry.m30 = 0.0f   ; ry.m31 = 0.0f; ry.m32 = 0.0f    ; ry.m33 = 1.0f;

        rz.m00 = (float)Math.cos(z); rz.m01 = -(float)Math.sin(z); rz.m02 = 0.0f; rz.m03 = 0.0f;
        rz.m10 = (float)Math.sin(z); rz.m11 = (float)Math.cos(z) ; rz.m12 = 0.0f; rz.m13 = 0.0f;
        rz.m20 = 0.0f   ; rz.m21 = 0.0f    ; rz.m22 = 1.0f; rz.m23 = 0.0f;
        rz.m30 = 0.0f   ; rz.m31 = 0.0f    ; rz.m32 = 0.0f; rz.m33 = 1.0f;
        RotateTrans = MathUtils.mul(RotateTrans, rx);
        RotateTrans = MathUtils.mul(RotateTrans, ry);
        RotateTrans = MathUtils.mul(RotateTrans, rz);

        modelMatrix = MathUtils.mul(modelMatrix, TranslationTrans);
        modelMatrix = MathUtils.mul(modelMatrix, RotateTrans);
        modelMatrix = MathUtils.mul(modelMatrix, ScaleTrans);

        return modelMatrix;
    }

    private void init() {
        Vector3f HTarget = new Vector3f(target.x, 0.0f, target.z);
        HTarget.normalise(HTarget);

        if (HTarget.z >= 0.0f){
            if (HTarget.x >= 0.0f){
                hAngle = 360.0f - MathUtils.radToDegrees((float)Math.asin(HTarget.z));
            }
            else{
                hAngle = 180.0f + MathUtils.radToDegrees((float)Math.asin(HTarget.z));
            }
        }
        else{
            if (HTarget.x >= 0.0f){
                hAngle = MathUtils.radToDegrees((float)Math.asin(-HTarget.z));
            }
            else{
                hAngle = 90.0f + MathUtils.radToDegrees((float)Math.asin(-HTarget.z));
            }
        }

        vAngle = -MathUtils.radToDegrees((float)Math.asin(HTarget.y));

        mousePosition = new Vector2f(displaySettings.getWidth() / 2, displaySettings.getHeight() / 2);
        Mouse.setCursorPosition((int)mousePosition.x, (int)mousePosition.y);
    }

    public void clearPosition() {
        position = new Vector3f(0.0f, 0.0f, 0.0f);
        target = new Vector3f(0.0f, 0.0f, 1.0f);
        up = new Vector3f(0.0f,1.0f, 0.0f);
        init();
    }

    public void onRender(int x, int y)
    {
        boolean shouldUpdate = false;
        int dx = (int)mousePosition.x - x;
        int dy = (int)mousePosition.y - y;
        if (Math.abs(dx) > MARGIN) {
            if (dx > 0) {
                hAngle -= 1f;
                shouldUpdate = true;
            }
            if (dx < 0) {
                hAngle += 1f;
                shouldUpdate = true;
            }
        }

        if (Math.abs(dy) > MARGIN) {
            if (dy < 0) {
                if (vAngle > -90.0f) {
                    vAngle -= 1f;
                    shouldUpdate = true;
                }
            }
            if (dy > 0) {
                if (vAngle < 90.0f) {
                    vAngle += 1f;
                    shouldUpdate = true;
                }
            }
        }

        if (shouldUpdate){
            update();
            Mouse.setCursorPosition(displaySettings.getWidth() / 2, displaySettings.getHeight() / 2);
        }
    }

    private void update() {
        Vector3f vAxis = new Vector3f(0.0f, 1.0f, 0.0f);

        // Rotate the view vector by the horizontal angle around the vertical axis
        Vector3f view = new Vector3f(1.0f, 0.0f, 0.0f);
        MathUtils.rotate(view, hAngle ,vAxis);
        view.normalise(view);

        // Rotate the view vector by the vertical angle around the horizontal axis
        Vector3f hAxis = Vector3f.cross(vAxis, view, null);
        hAxis.normalise(hAxis);
        MathUtils.rotate(view, vAngle, hAxis);
        view.normalise(view);

        target = view;
        target.normalise();

        up = Vector3f.cross(target, hAxis, null);
        up.normalise();
    }

    public Matrix4f createViewMatrix() {
        Matrix4f view = new Matrix4f();
        view.m03 = -position.x;
        view.m13 = -position.y;
        view.m23 = -position.z;
        return view;
    }
}