package org.engine;

import org.engine.display.DisplayManager;
import org.engine.display.DisplaySettings;
import org.lwjgl.util.vector.Vector3f;

public class Application {

  public static void main(String[] args) {
    Application app = new Application();
    app.run();
  }

  public void run() {
    DisplaySettings displaySettings = new DisplaySettings(800, 600, 60, "Engine");
    DisplayManager displayManager = new DisplayManager(displaySettings);
    Vertex[] vertexes = new Vertex[4];
    vertexes[0] = new Vertex(new float[] {-1.0f, -1.0f, 0.5773f, 1.0f},
        new float[] {1.0f, 0.0f, 0.0f, 1.0f}, new float[] {0.0f, 0.0f});
    vertexes[1] = new Vertex(new float[] {0.0f, -1.0f, -1.15475f, 1.0f},
        new float[] {0.0f, 1.0f, 0.0f, 1.0f}, new float[] {0.0f, 1.0f});
    vertexes[2] = new Vertex(new float[] {1.0f, -1.0f, 0.5773f, 1.0f},
        new float[] {0.0f, 0.0f, 1.0f, 1.0f}, new float[] {1.0f, 1.0f});
    vertexes[3] = new Vertex(new float[] {0.0f, 1.0f, 0.0f, 1.0f}, new float[] {1.0f, 1.0f, 1.0f, 1.0f},
            new float[] {1.0f, 0.0f});

    byte[] indices = new byte[] {
            0, 3, 1,
            1, 3, 2,
            2, 3, 0,
            0, 1, 2 };

    Mesh quad = new Mesh(vertexes, indices);
    Mesh quad2 = new Mesh(vertexes, indices);
    quad2.setModelPos(new Vector3f(0.0f, 1.0f, 3.0f));

    displayManager.initDisplay();
    displayManager.showDisplay(new Mesh[] {quad2});
    displayManager.closeDisplay();
  }
}
